package main.scala

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import java.io.File
import scala.collection.mutable.Map
import java.io.BufferedWriter
import java.io.FileWriter
import java.io.BufferedReader
import java.io.FileReader
import java.nio.charset.StandardCharsets
import java.io.OutputStreamWriter
import java.io.FileOutputStream

object DataFilter {
  def main(args: Array[String]): Unit = {
    try {
      if (args.length < 4 || args.length == 0 || args(0).equals("-h") || args(0).equals("--help"))
        printHelp
      else {
        // input arguments
        println("Getting user parameters...")
        val params = new ParamsHelper
        val inputPath = params.checkAndGetInput(args, "-i", "--input", ParamsHelperType.STRING).asInstanceOf[String]
        val outputPath = params.checkAndGetInput(args, "-o", "--output", ParamsHelperType.STRING).asInstanceOf[String]
        if (inputPath == null || outputPath == null) throw new Exception("ERROR: You must declare input and output")
        // Processing
        println("Processing...")
        val inputTopicFolders = (new File(inputPath)).listFiles()

        println("Input path: " + inputPath)
        println("Output path: " + outputPath)

        // Analyzing
        println("Processing ...")
        val specialChars = Array((" "), (";"), ("/"), ("."), (","), ("\""), ("\t"), ("#"), ("\u00a0"), ("("), (")"), ("["), ("]"), ("!"), ("?"), ("'"), (":"), ("&"), ("="), ("-"), ("<"), (">"), ("–"), ("{"), ("}"), ("\\"), ("..."), ("*"), ("+"), ("$"), ("@"), ("\u00a9"), ("\u00d7"), ("\u00ae"), ("\u00ad"), ("\u2028"), ("\u0323"), ("\u0300"), ("\u0301"), ("\u0302"), ("\u0303"), ("\u0309"), ("”"), ("“"), ("\""), ("\","), ("\":"), (":/"), ("\">"), ("=\"\""), ("=\"\">"), ("=\""), ("\"."), ("?\""), ("=”"), ("=“"), ("”,"), ("”."), ("“,"), ("“."), ("〞,"), ("〞."), ("〝,"), ("〝."), ("?”."), ("://"), ("_"), ("…"), ("’"), ("--"))
        val specialWords = Array(("ả"), ("ế"), ("ị"), ("ổ"), ("ở"), ("ợ"), ("ú"), ("ủ"), ("ụ"))
        //~~~~~~~~~~ Processing ~~~~~~~~~~
        inputTopicFolders.foreach(topicFolder => {
          val outputTopicDir = outputPath + File.separator + topicFolder.getName
          val outputTopicFolder = new File(outputTopicDir)
          if (!outputTopicFolder.exists) outputTopicFolder.mkdirs()
          val listFiles = topicFolder.listFiles()
          listFiles.foreach(file => {
            var words = Source.fromFile(file)("UTF-8").getLines().toArray
            words = words.filter { word => (word.length > 1 && !specialChars.contains(word)) || specialWords.contains(word) }

            val outFile = new File(outputTopicDir + File.separator + file.getName)
            val bwOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile, true), "UTF-8"))
            bwOut.flush()
            words.foreach { word => bwOut.write(word + "\n") }
            bwOut.close()
          })
        })
        println("Finished!!!")
      }
    } catch {
      case e: Exception => {
        e.printStackTrace()
        printHelp()
      }
    }
  }

  def printHelp() = {
    println("Usage: DataFilter [Arguments]")
    println("       Arguments:")
    println("              -i --input     [path]   : Path of corpus folder")
    println("              -o --output    [path]   : Output folder path")
    println("              -h --help               : Print this help")
  }
}