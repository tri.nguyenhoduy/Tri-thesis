package main.scala.model

import scala.collection.mutable.ArrayBuffer
import main.scala.TFIDFCalc
import scala.collection.mutable.Map

class Topic(var label: String, var docs: ArrayBuffer[Document], var size: Int) {
  var tfidf = new ArrayBuffer[Map[String, Double]]()
  def this() {
    this("", new ArrayBuffer[Document], 0)
  }

  def this(label: String) {
    this(label, new ArrayBuffer[Document], 0)
  }

  def this(docs: ArrayBuffer[Document]) {
    this("", docs, docs.size)
  }

  def addDocument(document: Document) {
    docs.append(document)
    size += 1
  }

  def statTFIDF() {
    docs.foreach { document =>
      var tfidfOneDoc = Map[String, Double]()
      document.words.foreach(oneWord => {
        if (oneWord != null)
          tfidfOneDoc += oneWord._1 -> TFIDFCalc.tfIdf(oneWord, document, docs)
      })
      tfidf.append(tfidfOneDoc)
    }
  }

  def getVocabulary(top: Int): ArrayBuffer[(String, Double)] = {
    statTFIDF()
    var vocabulary = tfidf.flatMap(word => word)
    vocabulary.sortWith((x, y) => {
      x._2 > y._2
    }).take(top)
  }

  def getVocabulary(lowerBound: Double, upperBound: Double): ArrayBuffer[(String, Double)] = {
    statTFIDF()
    var vocabulary = tfidf.flatMap(word => word)
    vocabulary.filter(x => {
      x._2 >= lowerBound && x._2 <= upperBound
    })
  }
}