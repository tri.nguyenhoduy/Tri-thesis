package main.scala

import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import java.io.File
import scala.collection.mutable.Map
import java.io.BufferedWriter
import java.io.FileWriter
import java.io.BufferedReader
import java.io.FileReader
import java.nio.charset.StandardCharsets
import java.io.OutputStreamWriter
import java.io.FileOutputStream
import java.util.StringTokenizer

object ReutersDataNormalizer {
  def main(args: Array[String]): Unit = {
    try {
      if (args.length < 6 || args.length == 0 || args(0).equals("-h") || args(0).equals("--help"))
        printHelp
      else {
        // input arguments
        println("Getting user parameters...")
        val params = new ParamsHelper
        val inputPath = params.checkAndGetInput(args, "-i", "--input", ParamsHelperType.STRING).asInstanceOf[String]
        val outputPath = params.checkAndGetInput(args, "-o", "--output", ParamsHelperType.STRING).asInstanceOf[String]
        val stopwordsPath = params.checkAndGetInput(args, "-sw", "--stopwords", ParamsHelperType.STRING).asInstanceOf[String]
        if (inputPath == null || outputPath == null || stopwordsPath == null) throw new Exception("ERROR: You must declare input, output and path to stopwords file!")
        // Processing
        println("Processing...")
        val inputTopicFolders = (new File(inputPath)).listFiles()

        println("Input path: " + inputPath)
        println("Output path: " + outputPath)

        // Analyzing
        println("Processing ...")
        val specialChars = Array((" "), (";"), ("/"), ("."), (","), ("\""), ("\t"), ("#"), ("\u00a0"), ("("), (")"), ("["), ("]"), ("!"), ("?"), ("'"), (":"), ("&"), ("="), ("-"), ("<"), (">"), ("–"), ("{"), ("}"), ("\\"), ("..."), ("*"), ("+"), ("$"), ("@"), ("\u00a9"), ("\u00d7"), ("\u00ae"), ("\u00ad"), ("\u2028"), ("\u0323"), ("\u0300"), ("\u0301"), ("\u0302"), ("\u0303"), ("\u0309"), ("”"), ("“"), ("\""), ("\","), ("\":"), (":/"), ("\">"), ("=\"\""), ("=\"\">"), ("=\""), ("\"."), ("?\""), ("=”"), ("=“"), ("”,"), ("”."), ("“,"), ("“."), ("〞,"), ("〞."), ("〝,"), ("〝."), ("?”."), ("://"), ("_"), ("…"), ("’"), ("--"))
        val stopWords = Source.fromFile(stopwordsPath)("UTF-8").getLines().toArray

        val firstChars = Array(('"'), ('''), ('('), ('<'), ('#'), ('.'), ('”'), ('“'))
        val firstChars2 = Array(("</"), ("&#"))
        val lastChars = Array(('"'), ('''), (')'), ('>'), (','), ('.'), ('?'), ('!'), (';'), ('”'), ('“'))
        val lastChars2 = Array((",\""), (">,"), (".,"), (".>"), (".\""), ("),"))

        //~~~~~~~~~~ Processing ~~~~~~~~~~
        inputTopicFolders.foreach(topicFolder => {
          val outputTopicDir = outputPath + File.separator + topicFolder.getName
          val outputTopicFolder = new File(outputTopicDir)
          if (!outputTopicFolder.exists) outputTopicFolder.mkdirs()
          val listFiles = topicFolder.listFiles()
          listFiles.foreach(file => {
            //println(file.getCanonicalPath)
            var lines = Source.fromFile(file)("UTF-8").getLines().toArray
            var words = new ArrayBuffer[String]
            lines.foreach(line => {
              val tokens = new StringTokenizer(line.toLowerCase.trim, " ")
              while (tokens.hasMoreTokens()) {
                var w = tokens.nextToken()
                if (w.length > 1) {
                  //println("First:\t\\u%04X".format(w(0).toInt))
                  //println("Last:\t\\u%04X".format(w(w.length - 1).toInt))
                  // trim first char
                  if (firstChars2.contains(w.slice(0, 2))) {
                    w = w.slice(2, w.length)
                  }
                  if (firstChars.contains(w(0))) {
                    w = w.slice(1, w.length)
                  }

                  // trim last char
                  if (lastChars2.contains(w.slice(w.length - 2, w.length))) {
                    w = w.slice(0, w.length - 2)
                  }
                  if (lastChars.contains(w(w.length - 1))) {
                    w = w.slice(0, w.length - 1)
                  }

                  if (!specialChars.contains(w) && !stopWords.contains(w)) {
                    words.append(w)
                  }
                }
              }
            })

            // file has less than 20 words is useless
            if (words.length >= 20) {
              //val outFile = new File(outputPath + File.separator + topicFolder.getName + "_" + file.getName)
              val outFile = new File(outputTopicDir + File.separator + topicFolder.getName + "_" + file.getName)
              val bwOut = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile, true), "UTF-8"))
              bwOut.flush()
              words.foreach { word => bwOut.write(word + "\n") }
              bwOut.close()
            }
          })
        })
        println("Finished!!!")
      }
    } catch {
      case e: Exception => {
        e.printStackTrace()
        printHelp()
      }
    }
  }

  def printHelp() = {
    println("Usage: ReutersDataNormalizer [Arguments]")
    println("       Arguments:")
    println("              -i  --input     [path]   : Path of corpus folder")
    println("              -o  --output    [path]   : Output folder path")
    println("              -sw --stopwords [path]   : Path to stopwords file")
    println("              -h  --help               : Print this help")
  }
}